import urllib2, json, sqlite3

SCRAPE_URL = 'http://pastebin.com/api_scraping.php?limit=500'
PASTE_URL = 'http://pastebin.com/api_scrape_item.php?i=%s'
CREATE_TBL = '''
            CREATE TABLE IF NOT EXISTS
            paste_data(id INTEGER PRIMARY KEY,
            title TEXT,
            syntax TEXT,
            scrape_url TEXT,
            expire_date INT,
            user TEXT,
            key TEXT,
            date INT,
            full_url TEXT,
            size INT,
            content BLOB)
            '''

def main():
    sql = sqlite3.connect('pastebin.db')
    sql.text_factory = str
    cur = sql.cursor()
    cur.execute(CREATE_TBL)
    sql.commit()

    pastes = json.loads(urllib2.urlopen(SCRAPE_URL).read())
    for p in pastes:
        cur.execute('SELECT * FROM paste_data WHERE key=?', (p['key'],))
        if not cur.fetchone():
            paste_content = urllib2.urlopen(PASTE_URL % p['key']).read()                        
            cur.execute('INSERT INTO paste_data(title, syntax, scrape_url, expire_date, user, key, date, full_url, size, content) VALUES(?,?,?,?,?,?,?,?,?,?)', (p['title'], p['syntax'], p['scrape_url'], p['expire'], p['user'], p['key'], p['date'], p['full_url'],  p['size'], paste_content))
            sql.commit()

if __name__ == '__main__':
    main()           